/*************************************************************
* BASIC PERCEPTRON 
* Created by Aman Singh, June 2016
* 
*************************************************************/

#include <cstdlib>

class perceptron {
public:
	perceptron (float w0, float w1, float w2, float lrate);
	void learn (float x1, float x2, bool out);
	float calculate (float x1, float x2);
	float getw1();
	float getw2();
	float getw0();
	float getlrate();
private:
	float w0;
	float w1;
	float w2;
	float lrate;
};