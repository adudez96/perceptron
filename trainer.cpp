/*************************************************************
* PERCEPTRON TRAINER
* Created by Aman Singh, June 2016
* 
*************************************************************/

#include <cstdio>
#include <cstdlib>
#include "perceptron.hpp"

#define PARAMS_STRING_SIZE 256

void printHelpText();

int main (int argc, char ** argv) {

	// display help text
	printHelpText();

	// make perceptron
	// enter random values for weights
	perceptron * perc = new perceptron(0.5, 1.0, 2.0, 0.5);

	printf("Perceptron created with initial values: \n");
	printf("     w0 = %f\n", perc->getw0());
	printf("     w1 = %f\n", perc->getw1());
	printf("     w2 = %f\n", perc->getw2());
	printf("  lrate = %f\n", perc->getlrate());

	bool run = true;
	while (run) {
		// get input 
		char params[PARAMS_STRING_SIZE];
		printf("||=>  ");
		scanf("%s",params);

		// on quit
		switch (params[0]) {
			case 'q':
				run = false;
				break;
			case 'h':
				printHelpText();
				break;
			case 't':
				// TRAIN
				break;
			case 'e':
				// EVALUATE
				break;
			default:
				printHelpText();
				break;
		}

		// run training loop
	}


	// free memory
	puts("Freeing memory...");
	delete perc;

	puts("Exiting successfully.");
	return EXIT_SUCCESS;
}


void printHelpText () {
	puts("################################################################");
	puts("# BASIC PERCEPTRON TRAINER ");
	puts("# Created by Aman Singh, June 2016");
	puts("# ");
	puts("# COMMANDS: ");
	puts("#  - 'h':                   display help text");
	puts("#  - 't <x1> <x2> <class>': train perceptron with inputs and");
	puts("#                             expected class");
	puts("#  - 'e <x1> <x2>':         make the perceptron evaluate the");
	puts("#                             class of the inputs given");\
	puts("#  - 'q':                   quit");
	puts("# ");
	puts("# ");
	puts("################################################################");
}






