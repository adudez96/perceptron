/*************************************************************
* BASIC PERCEPTRON
* Created by Aman Singh, June 2016
* 
*************************************************************/

#include <cstdlib>
#include "perceptron.hpp"

#define SUCCESS_RATE_MODIFIER 0.91
#define FAILURE_RATE_MODIFIER 0.96

perceptron::perceptron (float w0, float w1, float w2, float lrate) {
	this->w0 = w0;
	this->w1 = w1;
	this->w2 = w2;
	this->lrate = lrate;
}

void perceptron::learn (float x1, float x2, bool out) {
	float myout = (w1 * x1) + (w2 * x2) + w0;
	
	// +ve class expected
	if (myout >= 0 && out ) {		
		// classes match, no change
		lrate = lrate * SUCCESS_RATE_MODIFIER;
	}
	if (myout < 0 && out ) {
		// classes don't match: add
		w1 = w1 + (x1*lrate);
		w2 = w2 + (x2*lrate);
		w0 = w0 + lrate;
		lrate = lrate * FAILURE_RATE_MODIFIER;
	}

	// -ve class expected
	if (myout < 0 && !out) {
		// classes match, no change
		lrate = lrate * SUCCESS_RATE_MODIFIER;
	}
	if (myout >= 0 && !out) {
		// classes don't match: subtract
		w1 = w1 - (x1*lrate);
		w2 = w2 - (x2*lrate);
		w0 = w0 - lrate;
		lrate = lrate * FAILURE_RATE_MODIFIER;
	}
}

float perceptron::calculate (float x1, float x2) {
	return (w1 * x1) + (w2 * x2) + w0;
}

float perceptron::getw0() {
	return w0;
}

float perceptron::getw1() {
	return w1;
}

float perceptron::getw2() {
	return w2;
}

float perceptron::getlrate() {
	return lrate;
}
