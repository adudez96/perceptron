CC=g++
CFLAGS=-c -Wall -Werror -O

all: main

main: trainer.o perceptron.o
	$(CC) trainer.o perceptron.o -o trainer

trainer.o: trainer.cpp
	$(CC) $(CFLAGS) trainer.cpp

perceptron.o: perceptron.cpp perceptron.hpp
	$(CC) $(CFLAGS) perceptron.cpp perceptron.hpp

clean:
	rm *.o main